function quicksort(list, pivotFunction, compareFunction) {
	var lower = [];
	var equal = [];
	var higher = [];
	var pivot = 0;
	var pivotValue = 0;
	
	if (!list || (list.length === 0)) {
		return [];
	}

	if (!pivotFunction) {
		pivotFunction = function (list) {
			return Math.floor(list.length / 2);
		}
	}
	
	if (!compareFunction) {
		compareFunction = function (a, b) {
			return a - b;
		}
	}

	pivot = pivotFunction(list);
	pivotValue = list[pivot];
	
	for (var i = 0; i < list.length; i++) {
		if (compareFunction(list[i], pivotValue) < 0) {
			lower.push(list[i]);
		} else if (compareFunction(list[i], pivotValue) > 0) {
			higher.push(list[i]);
		} else {
			equal.push(list[i]);
		}
	}
	
	return quicksort(lower, pivotFunction, compareFunction).concat(equal, quicksort(higher, pivotFunction, compareFunction));
}
